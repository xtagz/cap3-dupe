import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';  
import { Navigate } from 'react-router-dom'; 
import Swal from 'sweetalert2';
import '../App.css';


export default function Login() {


    // 3 - use the state
    // variable, setter function
    // const { user, setUser } = useContext(UserContext); // 
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


  
    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);
            console.log('Check accessToken');
            console.log(data.accessToken);
           

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Enjoy!!"
                })
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try agin."
                })
            }
           
        })
        setEmail('');
    }
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
            localStorage.setItem("userId", data._id);
            localStorage.setItem("userRole", data.isAdmin);

            console.log("Login: ");
            console.log(user)

        })
    }

    useEffect(() => {
            // Validation to enable submit button when all fields are populated and both passwords match
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);


    return (
        (user.id !== null)
        ? // true - means email field is successfully set
        <Navigate to="/products"  />
        : // false - means email field is not succesfully set
        <div className='login-form'>
        <Form onSubmit = {(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

             { isActive ?  // true
                <Button className='mt-3' variant="success" type="submit" id="submitBtn">
                    LOGIN
                </Button>
                : // false
                <Button className='mt-3' variant="success" type="submit" id="submitBtn" disabled>
                    LOGIN
                </Button>
            }

        </Form>
        </div>
    )
}


