import { Fragment } from 'react';
import { Carousel } from 'react-bootstrap';
import soccer from './images/soccer.png';
import hoodies from './images/hoodies.png';
import jersey from './images/jersey.png';
import ecommercebanner from './images/ecommercebanner.jpg';
import '../App.css';

export default function Home() {
	return (
		<Fragment>
			<div >
				<img
					className="d-block  mx-auto"
					src={ecommercebanner}
					alt='banner'
				/>
			</div>
			<Carousel fade>
				<Carousel.Item>
					<img
						className="d-block  mx-auto"
						src={hoodies}
						alt="First slide"
					/>
				</Carousel.Item>
				<Carousel.Item>
					<img
						className="d-block mx-auto"
						src={jersey}
						alt="Second slide"
					/>
				</Carousel.Item>
				<Carousel.Item>
					<img
						className="d-block  mx-auto"
						src={soccer}
						alt="Third slide"
					/>
				</Carousel.Item>
			</Carousel>
		</Fragment>
	);
}
