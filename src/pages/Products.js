import { Fragment, useState } from 'react';
import { Navigate } from "react-router-dom";
import { useEffect, useContext } from "react";
import ProductCard from '../components/ProductCard';
import { Container, Form, DropdownButton, Dropdown  } from 'react-bootstrap'; // import Form component from react-bootstrap
import UserContext from '../UserContext';
import '../Product.css';

export default function Product(){
  const { user } = useContext(UserContext);
  const [products, setProduct] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
	const [selectedCategory, setSelectedCategory] = useState('');

  useEffect(() =>{
    if (searchTerm) {
      fetch(`${process.env.REACT_APP_API_URL}/products/filter`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          name: searchTerm
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (Array.isArray(data)) {
          setProduct(data.map(product => {
            return(
              <ProductCard key={product._id} productProp={product}/>
            );
          }));
        } else {
          setProduct([]);
        }
      })
    }
		 else if (selectedCategory) {
      fetch(`${process.env.REACT_APP_API_URL}/products/category`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          category: selectedCategory
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (Array.isArray(data)) {
          setProduct(data.map(product => {
            return(
              <ProductCard key={product._id} productProp={product}/>
            );
          }));
        } else {
          setProduct([]);
        }
      })
    }
		 else {
      fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (Array.isArray(data)) {
          setProduct(data.map(product =>{
            return(
              <ProductCard key={product._id} productProp={product}/>
            );
          }));
        } else {
          setProduct([]);
        }
      });
    }
  }, [searchTerm,selectedCategory]);

  return(
    (user.isAdmin)
    ?
      <Navigate to="/admin" />
    : 
    <>
      <Container>
        <h1 className='text-center header-text'>PRODUCTS</h1>
        <div className="search-box-container d-flex justify-content-center">
          <Form.Control className='search-bar' type='text' placeholder='Search' onChange={(e) => setSearchTerm(e.target.value)} />
					<DropdownButton id="dropdown-basic-button" variant='dark' title="Category">
            <Dropdown.Item onClick={() => setSelectedCategory('jacket')}>Jacket</Dropdown.Item>
            <Dropdown.Item onClick={() => setSelectedCategory('basketball jersey')}>Basketball Jersey</Dropdown.Item>
            <Dropdown.Item onClick={() => setSelectedCategory('football jersey')}>Football Jersey</Dropdown.Item>
            <Dropdown.Item onClick={() => setSelectedCategory('headgear')}>Headgear</Dropdown.Item>
            <Dropdown.Item onClick={() => setSelectedCategory('poloshirt')}>Poloshirt</Dropdown.Item>
          </DropdownButton>
        </div>
        {products}
      </Container>
    </>
  )
}
