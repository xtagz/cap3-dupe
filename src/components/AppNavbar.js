import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { FormControl } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import { Fragment, useContext,useState } from 'react';
import UserContext from '../UserContext';
import '../Navbar.css';

export default function AppNavbar(){
    const { user, setUser } = useContext(UserContext);
    console.log(user);

    const [userId, setUSerId] = useState(localStorage.getItem("userId"));  
    console.log(userId);

    return(
        <Navbar bg="dark" expand="md">
            <Container fluid>
                <Navbar.Brand as={Link} to="/" href="#home" >Ak Apparel</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                        
                <Navbar.Collapse id="basic-navbar-nav" className='justify-content-end'>
                        
                    <Nav>
                        <Nav.Link as={NavLink} to="/products" >Products</Nav.Link>    
                        {(user.id === null )?                                    
                        <Fragment>
                            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                        </Fragment>
                            :                                   
                            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                        }
                                
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
